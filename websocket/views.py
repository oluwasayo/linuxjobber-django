import json
from django.db import connection
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Connection, ChatMessage

def _parse_body(body):
    body_unicode = body.decode("utf-8")
    return json.loads(body_unicode)
# Create your views here.
@csrf_exempt
def test(request):
    return JsonResponse({"msg": "Hello Daud"}, status=200)

@csrf_exempt
def connect(request):
    body = _parse_body(request.body)
    print(body)
    connection_id = body["connectionId"]
    Connection.objects.create(connection_id=connection_id)
    return JsonResponse({"msg": "Connect successfully"}, status=200)

@csrf_exempt
def disconnet(request):
    body = _parse_body(request.body)
    print(body)
    connection_id = body["connectionId"]
    Connection.objects.delete(connetion_id=connection_id)
    return JsonResponse({"msg": "Disconnected successfully"}, status=200)