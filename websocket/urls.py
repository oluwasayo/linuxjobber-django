from django.urls import path
from .views import *
import json
from .models import ChatMessage, Connection

app_name = "websocket"

urlpatterns = [
    path("test/", test, name="test"),
    path("connect/", connect, name="connect"),
    path("disconnect/", connect, name="disconnect"),
]