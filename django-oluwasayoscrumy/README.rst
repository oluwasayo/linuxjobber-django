================
Oluwasayo Scrumy
===============

Oluwasayo scrumy is a simple Django app built by Oluwasayo
while learning about Django with Linuxjobber.

You can find detailed documentation in the "docs" directory.

Quick Start
-----------

1. Add "oluwasayoscrumy" to your INSTALLED_APPS in your settings.py file 
like this:

	INSTALLED_APPS = [
		...,
		'oluwasayoscrumy',
	]

2. Include the oluwasayoscrumy URLConf in your project urls.py like this:
	path('oluwasayoscrumy/', include('oluwasayoscrumy.urls')),

3. Run 'python manage.py migrate' to create the oluwasayoscrumy models.

4. Start the development server and visit http://localhost:8000/oluwasayoscrumy/
