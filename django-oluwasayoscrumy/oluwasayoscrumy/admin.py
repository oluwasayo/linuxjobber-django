from django.contrib import admin
from .models import GoalStatus, ScrumyHistory, ScrumyGoal

# Register your models here.
admin.site.register([GoalStatus, ScrumyHistory, ScrumyGoal])
