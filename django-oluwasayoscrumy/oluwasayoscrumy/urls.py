from django.urls import path, include
from .views import index, move_goal, home, add_goal

app_name = "oluwasayoscrumy"


urlpatterns = [
    path("", index, name="index"),
    path("movegoal/<int:goal_id>", move_goal, name="move-goal"),
    path("addgoal/", add_goal, name="add-goal"),
    path("accounts/", include("django.contrib.auth.urls") ),
    path("home/", home, name="home"),
]
