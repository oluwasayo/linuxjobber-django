import random
from django.http.request import HttpRequest
from django.shortcuts import render
from django.http import HttpResponse
from .models import ScrumyGoal, GoalStatus
from django.contrib.auth.models import User

# Create your views here.


def index(request):
    data = ScrumyGoal.objects.filter(goal_name="Learn Django").first()
    context = {"data": data.goal_name}
    return render(request, "oluwasayoscrumy/index.html", context)


def move_goal(request, goal_id):
    try:
        data = ScrumyGoal.objects.get(goal_id=goal_id).goal_name
    except ScrumyGoal.DoesNotExist:
        data = "Goal with ID {} does not exist".format(goal_id)
        context = { "error": data }
        return render(request, 'oluwasayoscrumy/exception.html', context)
    except:
        data = "Invalid ID"
    return HttpResponse(f"<h1>Goal: {data}</h1>")


def add_goal(request):
    goal_id = random.randrange(1000, 9999)
    while ScrumyGoal.objects.filter(goal_id=goal_id):
        goal_id = random.randrange(1000, 9999)
    usernames = ["louis", "oluwasayo"]
    user = User.objects.get(username=random.choice(usernames))
    goal_status = GoalStatus.objects.get(status_name="Weekly Goal")
    goals = ["Learn Django", "Keep Learning Django"]
    ScrumyGoal.objects.create(
        goal_name=random.choice(goals),
        goal_id=goal_id,
        created_by="Louis",
        moved_by="Louis",
        goal_status=goal_status,
        user=user,
    )
    return HttpResponse("Created Goal")


def home(request):
    response = []
    users = User.objects.all()
    for user in users:
        response.append({
            'user': user, 
            'weekly_goals': user.goals.filter(goal_status__status_name="Weekly Goal"),
            'daily_goals': user.goals.filter(goal_status__status_name="Daily Goal"),
            'verify_goals': user.goals.filter(goal_status__status_name="Verify Goal"),
            'done_goals': user.goals.filter(goal_status__status_name="Done Goal"),
            })

    context = {
        "users": response
    }
   
    return render(request, 'oluwasayoscrumy/home.html', context)
