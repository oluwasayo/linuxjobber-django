import random
from django.http.request import HttpRequest
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.http import HttpResponse
from .models import ScrumyGoal, GoalStatus
from django.contrib.auth.models import User, Group
from .forms import MoveGoalForm, SignUpForm, CreateGoalForm
from django.urls import reverse
from django.views import generic

# Create your views here.


def index(request):
    form = SignUpForm()
    if request.method == "POST":
        form = SignUpForm(request.POST)

        if form.is_valid():
            user = form.save()
            group = Group.objects.get(name="Developer")
            group.user_set.add(user)
            group.save()

            return HttpResponseRedirect(reverse("oluwasayoscrumy:created"))

    else:
        form = SignUpForm()
    
    context = {"data": "Sign Up", "form": form, "current_user": request.user}
    return render(request, "oluwasayoscrumy/index.html", context)


def move_goal(request, goal_id):
    if request.method == "POST":
        goal = ScrumyGoal.objects.get(goal_id=goal_id)
        form = MoveGoalForm(request.POST, instance=goal)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("oluwasayoscrumy:home"))
    else:
        try:
            goal = ScrumyGoal.objects.get(goal_id=goal_id)
            form = MoveGoalForm(instance=goal)
        except ScrumyGoal.DoesNotExist:
            data = "Goal with ID {} does not exist".format(goal_id)
            return render(request, 'oluwasayoscrumy/exception.html', {"error": data })
    return render(request, "oluwasayoscrumy/move-goal.html", { "form": form, "goal_id": goal_id, "current_user": request.user})


def add_goal(request):
    if request.method == "POST":
        
        form = CreateGoalForm(request.POST)

        if form.is_valid():
            goal = form.save(commit=False)
            goal_id = random.randrange(1000, 9999)
            goal_status = GoalStatus.objects.get(status_name=random.choice(["Weekly Goal", "Daily Goal", "Done Goal", "Verify Goal" ]))
            while ScrumyGoal.objects.filter(goal_id=goal_id):
                goal_id = random.randrange(1000, 9999)
            goal.goal_id = goal_id
            goal.goal_status = goal_status
            goal.save()
            form.save_m2m()

            return HttpResponseRedirect(reverse("oluwasayoscrumy:add-goal"))
    else:
        form = CreateGoalForm()

    context = {"form": form, "current_user": request.user}
    return render(request, 'oluwasayoscrumy/add-goal.html', context=context)


def home(request):
    print(dir(request.user))
    print(request.user.groups)
    users = User.objects.all()
    context = {
        "users": users,
        "current_user": request.user
    }
    return render(request, 'oluwasayoscrumy/home.html', context)


def success(request):
    return render(request, "oluwasayoscrumy/create-success.html")

class SuccessView(generic.TemplateView):

    template_name = 'oluwasayoscrumy/create-success.html'