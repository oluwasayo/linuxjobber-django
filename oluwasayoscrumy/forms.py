from django import forms
from django.contrib.auth import models
from django.forms import fields
from .models import ScrumyGoal
from django.contrib.auth.models import User

class SignUpForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ["first_name", "last_name", "email",  "username", "password"]

class CreateGoalForm(forms.ModelForm):
    class Meta:
        model = ScrumyGoal
        fields = ["goal_name", "user", "goal_status"]

class MoveGoalForm(forms.ModelForm):
    class Meta:
        model = ScrumyGoal
        fields = ["goal_status",]