asgiref==3.4.1
Babel==2.5.1
cffi==1.11.5
chardet==3.0.4
# cloud-init==21.1
configobj==5.0.6
cryptography==3.2.1
dbus-python==1.2.4
decorator==4.2.1
Django==3.2.7
# gpg==1.13.1
idna==2.5
Jinja2==2.10.1
jsonpatch==1.21
jsonpointer==1.10
jsonschema==2.6.0
# libcomps==0.1.16
MarkupSafe==0.23
# mysqlclient==2.0.3
oauthlib==2.1.0
# perf==0.1
pexpect==4.3.1
ply==3.9
prettytable==0.7.2
ptyprocess==0.5.2
# pycairo==1.16.3
pycparser==2.14
pydbus==0.6.0
pygobject==3.28.3
PyJWT==1.6.1
pyserial==3.1.1
PySocks==1.6.8
python-dateutil==2.6.1
# python-linux-procfs==0.6.3
pytz==2021.1
pyudev==0.21.0
PyYAML==3.12
requests==2.20.0
# rpm==4.14.3
# schedutils==0.6
# selinux==2.9
# sepolicy==1.1
# setools==4.3.0
# setroubleshoot==1.1
six==1.11.0
# slip==0.6.4
# slip.dbus==0.6.4
# sos==4.1
sqlparse==0.4.1
# syspurpose==1.28.21
# systemd-python==234
typing-extensions==3.10.0.2
# ufw==0.35